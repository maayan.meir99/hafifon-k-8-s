### Goals
- The trainee will understand the different types of consules in a linux environment
- The trainee will know how to check who is connected to a machine

### Tasks
- Explain the following commands and their output
  - `tty`
  - `who`

### Read About

You shouldn't waste more than 3-4 hours on that!- Read about the following:

  - Physical Console 
  - Virtual consoles 
  - Remote Pseudo 
  - Explain the difference between Physical, Local Pseudo and Remote Pseudo TTY.
- Answer the following questions:
  - SSH is example to which kind of TTY?
  - How many physical consoles exist in a Linux environment?
  - What is the meaning of the number of files in the /dev/pts directory?
  - Login to your second physical tty, run the `who` command and exaplain the meaning of the output



