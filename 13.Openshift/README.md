# Openshift

### Intro and others
The difference between Openshift and Kubernetes
 - What is route, and what is the difference between Ingress Operator
 - What is the difference between operator and a controller?
 - Read about the [creation of ansible operators](https://learn.openshift.com/ansibleop/ansible-operator-overview/?extIdCarryOver=true&sc_cid=701f2000001OH7YAAW)

### Mgmt Operators

1. authentication
2. console
3. etcd
4. ingress
5. openshift-apiserver
6. machine-config
7. network
8. dns
9. image registry

### Network flow

Understand the network flow of a packet into the pod of an openshift cluster.


### Openshift Installation

How to install openshift on bare metal and vsphere
 - Igition files
 - bootstrap


### Tactic environment
The process of creating openshift cluster with Tower Workflow
Read the Troublshooting OCP doc in the tactic environment
