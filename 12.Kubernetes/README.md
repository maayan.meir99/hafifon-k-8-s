### Goals

- The trainee will understand k8s's components and architecture

  

### Tasks

- Read the following book: [Kubernetes in action](https://drive.google.com/file/d/1XVZp5QlZh3R9R--sPchcUV7kGlOUz6g4/view?usp=sharing)

  **More Sources**
  - [Certified Kubernetes Administrator course from udemy](https://drive.google.com/drive/folders/1GNXztUdtESDTt5uefyV7n5NpofaxLPn4?usp=sharing)

  - [The Kubernetes Book by Nigel Poulton (a shorter version)](https://drive.google.com/file/d/1tnHb9ijciKs6cI07XJrg4TUrCWjNtaUu/view)
